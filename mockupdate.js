"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timePartition = exports.randomDateForMonth = exports.dateLimitForMonth = exports["default"] = void 0;

var _random = _interopRequireWildcard(require("./random.js"));

var _isDate = _interopRequireDefault(require("date-fns/isDate"));

var _isValid = _interopRequireDefault(require("date-fns/isValid"));

var _parseISO = _interopRequireDefault(require("date-fns/parseISO"));

var _addMonths = _interopRequireDefault(require("date-fns/addMonths"));

var _subMonths = _interopRequireDefault(require("date-fns/subMonths"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var defaultTimePartition = Object.freeze({
  daytime: [7, 18],
  nighttime: [19, 23],
  midnight: [0, 6],
  morning: [7, 10],
  noon: [11, 13],
  afternoon: [12, 18]
});
exports.timePartition = defaultTimePartition;

var dateLimitForMonth = function dateLimitForMonth(m) {
  switch (m) {
    case 1:
      return 28;

    case 0:
    case 2:
    case 4:
    case 6:
    case 7:
    case 9:
    case 11:
      return 31;

    default:
      return 30;
  }
};

exports.dateLimitForMonth = dateLimitForMonth;

var randomDateForMonth = function randomDateForMonth(d) {
  var m = d.getMonth();
  var lim = dateLimitForMonth(m);
  var date = (0, _random.random)(1, lim);
  return new Date(d.getFullYear(), m, date, d.getHours(), d.getMinutes(), d.getSeconds());
};
/**
 * Random content generator with given pool of source.
 * @param {[Array]} pool Array
 */


exports.randomDateForMonth = randomDateForMonth;

var MockUpDate = function MockUpDate(ref) {
  if (!(this instanceof MockUpDate)) {
    return new MockUpDate(ref);
  }

  if (!(0, _isDate["default"])(ref)) {
    console.log("[MockUpDate] Trying to parse reference date", ref);
    ref = (0, _parseISO["default"])(ref);
  }

  if (!(0, _isValid["default"])(ref)) {
    throw Error("[MockUpDate] Reference date is not a valid date", ref);
  }

  var refYear = ref.getFullYear();
  var refMonth = ref.getMonth();
  var refDate = ref.getDate();
  var refHours = ref.getHours();
  var refMinutes = ref.getMinutes();
  var refSeconds = ref.getSeconds();
  console.log("[MockUpDate] initialized with:", refYear, refMonth, refDate, refHours, refMinutes, refSeconds);
  /**
   * Make the hour of generated date to be a random number. If the partition is given, then the generated hours will be
   * withing the range of the partition.
   */

  Object.defineProperty(this, "makeRandomHours", {
    get: function get() {
      return function (partition) {
        var hour;

        if (partition && partition.lenth === 2) {
          refHours = (0, _random.random)(partition[0], partition[1]);
        } else {
          refHours = (0, _random.random)(0, 23);
        }
      };
    },
    configurable: false,
    enumerable: false
  });
  Object.defineProperty(this, "setMinutesSeconds", {
    get: function get() {
      return function (min, sec) {
        refMinutes = min;
        refSeconds = sec;
        console.log("[setMinutesSeconds] to:", refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });
  Object.defineProperty(this, "sameDate", {
    get: function get() {
      return function () {
        // let lim = dateLimitForMonth(refMonth);
        // let date = random(1, lim);
        return new Date(refYear, refMonth, refDate, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });
  Object.defineProperty(this, "sameMonth", {
    get: function get() {
      return function () {
        var lim = dateLimitForMonth(refMonth);
        var date = (0, _random.random)(1, lim);
        return new Date(refYear, refMonth, date, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });
  Object.defineProperty(this, "lastMonth", {
    get: function get() {
      return function () {
        var pm = (0, _subMonths["default"])(new Date(refYear, refMonth), 1);
        var m = pm.getMonth();
        var lim = dateLimitForMonth(m);
        var date = (0, _random.random)(1, lim);
        return new Date(pm.getFullYear(), m, date, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });
  Object.defineProperty(this, "nextMonth", {
    get: function get() {
      return function () {
        var pm = (0, _addMonths["default"])(new Date(refYear, refMonth), 1);
        var m = pm.getMonth();
        var lim = dateLimitForMonth(m);
        var date = (0, _random.random)(1, lim);
        return new Date(pm.getFullYear(), m, date, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });
};

var _default = MockUpDate;
exports["default"] = _default;