
/// Default random number generator. The returned value is expected to be between 0 and 1.
const PRNG = Math.random;

/**
 * Generate random number from 0 to 100: randomScalingFactor(100)
 * @param  {[type]} m [description]
 * @return {[Integer]}   [description]
 */
const randomScalingFactor = function(m) {
    return Math.round(PRNG()*m);
}

/**
 * Generate random number from f to t: random(f, t)
 * @param  {[type]} f [description]
 * @param  {[type]} t [description]
 * @return {[Integer]}   [description]
 */
const random = function(f, t) {
    let len = t - f;
    return f + randomScalingFactor(len);
}

/**
 * Random content generator with given pool of source.
 * @param {[Array]} pool Array
 */
const RandomThis = function(pool) {

  if (!(this instanceof RandomThis)) {
      return new RandomThis(pool);
  }

  if (!pool || !pool.length || pool.length < 1) {
      throw Error("[randomThis] pool must be an array with contents");
  }

  let lim = pool.length - 1;

  Object.defineProperty(this, "gen", {
    get: function() {
      return function() {
        return pool[randomScalingFactor(lim)];
      }
    },
    configurable: false,
    enumerable: false
  });

}


/**
 * Partition 1 into n decimal fractions.
 * 
 * @param  {[type]} n [description]
 * @return {[type]}   [description]
 */
const randomPartition = function(n) {

  if(isNaN(n)) {
    throw new Error("n should be a number.")
  }

  if( n < 2 || (n % 1)!=0 ) {
    throw new Error("n must be a whole number greater than 1.")
  }

  /// Make n-1 points
  n--;
  let points = [];
  for(; n-- ;) {
    points.push(PRNG());
  }

  points.sort((a,b)=>a-b);
  points.push(1);

  var cursor = 0;
  return points.reduce((res,it)=>{
    res.push(it - cursor);
    cursor = it;
    return res;
  }, []);

}



export default randomScalingFactor
export { random, randomPartition, RandomThis }