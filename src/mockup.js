
import { RandomThis } from './random.js';
import MockUpAttr from './mockupattr.js';
import MockUpGen from './mockupgen.js';

/**
 * [MockUpCTOR description]
 * @param {[type]} properties [description]
 */
function MockUpCTOR (properties) {

    if (!(this instanceof MockUpCTOR)) {
        return new MockUpCTOR(properties);
    }

    /// ============================================================
    /// Initialization
    /// ============================================================


    if (!properties || !properties.length || properties.length < 1) {
        throw Error("[MockUpCTOR] properties must be an array with contents");
    }

    function gen() {

      console.log("[gen] properties ", properties);

      return properties.reduce(function(result, it){

        console.log("[gen][reduce] ", it);

        result[it.name] = it.gen();
        return result;

      }, {});

    }


    /// ============================================================
    /// API
    /// ============================================================

    /**
     * Generate n random data.
     * @return {[type]}               List of mock-up data
     */
    Object.defineProperty(this, "make", {
      get: function() {
        return function(n) {
          n = n || 1;
          var i, it;
          let list = [];
          i = n;
          for(; i-- ;) {
            list.push(gen());
          }
          return list;
        }
      },
      configurable: false,
      enumerable: false
    });


}



function MockUp(config) {

  if( config && config.length ) {
    return new MockUpCTOR(config);
  }

  if(typeof config == "object") {

    let properties = [];
    console.log("[config]", config);

    for( var prop in config ) {

      let pool = config[prop];
      console.log("[config]["+prop+"] pool", pool);

      if( pool ) {

        if( typeof pool == "function" ) {

          properties.push(new MockUpAttr(prop, pool));
          continue;

        } else if( pool.length ) {

          let mug = new MockUpGen(pool);
          properties.push(new MockUpAttr(prop, mug.one))
          continue;

        } else {

          console.warn("[config]["+prop+"] unknown pool type", pool);

        }

      }

    }

    return new MockUpCTOR(properties);

  }


  throw Error("[MockUp] config data must be an object or an array.");

}


export default MockUp
export { MockUpCTOR, MockUpAttr }