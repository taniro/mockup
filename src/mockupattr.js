

function MockUpAttr (name, gen) {

    if (!(this instanceof MockUpAttr)) {
        return new MockUpAttr();
    }

    if( typeof gen !== 'function' ) {
      throw Error("[MockUpAttr] gen must be a function that returns mock-up attribute data.");
    }

    Object.defineProperty(this, "name", {
      get: function() {
        return name;
      },
      configurable: false,
      enumerable: true
    });

    Object.defineProperty(this, "gen", {
      get: function() {
        return gen;
      },
      configurable: false,
      enumerable: true
    });

}

export default MockUpAttr