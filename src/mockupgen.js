
import { RandomThis } from './random.js';

function MockUpGen (pool) {

    if (!(this instanceof MockUpGen)) {
        return new MockUpGen(pool);
    }

    /// ============================================================
    /// Initialization
    /// ============================================================

    var gen;

    if( typeof pool === 'function' ) {

      gen = pool;

    } else {

      let rt = new RandomThis(pool);
      gen = rt.gen;

    }


    /// ============================================================
    /// API
    /// ============================================================

    Object.defineProperty(this, "one", {
      get: function() {
        return function() {
          return gen();
        }
      },
      configurable: false,
      enumerable: false
    });

    /**
     * Generate n random data.
     * @return {[type]}               List of mock-up data
     */
    Object.defineProperty(this, "make", {
      get: function() {
        return function(n) {
          var i, it;
          let list = [];
          i = n;
          for(; i-- ;) {
            list.push(gen());
          }
          return list;
        }
      },
      configurable: false,
      enumerable: false
    });

    /**
     * Generate a number of values and combine them as one single value.
     * @return {[String]}               [description]
     */
    Object.defineProperty(this, "coin", {
      get: function() {
        return function(n, sep) {
          sep = sep || "";
          return this.make(n).join(sep);
        }
      },
      configurable: false,
      enumerable: true
    });

}

export default MockUpGen