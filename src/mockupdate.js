
import randomScalingFactor, { random } from './random.js';

import isDate from 'date-fns/isDate';
import isValid from 'date-fns/isValid';

import parseISO from 'date-fns/parseISO';

import addMonths from 'date-fns/addMonths';
import subMonths from 'date-fns/subMonths';

const defaultTimePartition = Object.freeze({
  daytime: [7,18],
  nighttime: [19,23],
  midnight: [0,6],
  morning: [7,10],
  noon: [11,13],
  afternoon: [12,18],
});

const dateLimitForMonth = function (m) {
  switch (m) {
    case 1:
      return 28;

    case 0:
    case 2:
    case 4:
    case 6:
    case 7:
    case 9:
    case 11:
      return 31;

    default:
      return 30;
  }
};

const randomDateForMonth = function (d) {

  let m = d.getMonth();
  let lim = dateLimitForMonth(m);
  let date = random(1, lim);

  return new Date(d.getFullYear(), m, date, d.getHours(), d.getMinutes(), d.getSeconds());
};

/**
 * Random content generator with given pool of source.
 * @param {[Array]} pool Array
 */
const MockUpDate = function (ref) {

  if (!(this instanceof MockUpDate)) {
    return new MockUpDate(ref);
  }

  if (!isDate(ref)) {

    console.log("[MockUpDate] Trying to parse reference date", ref);

    ref = parseISO(ref);
  }

  if (!isValid(ref)) {
    throw Error("[MockUpDate] Reference date is not a valid date", ref);
  }

  let refYear = ref.getFullYear();
  let refMonth = ref.getMonth();
  let refDate = ref.getDate();

  var refHours = ref.getHours();
  var refMinutes = ref.getMinutes();
  var refSeconds = ref.getSeconds();

  console.log("[MockUpDate] initialized with:", refYear, refMonth, refDate, refHours, refMinutes, refSeconds);


  /**
   * Make the hour of generated date to be a random number. If the partition is given, then the generated hours will be
   * withing the range of the partition.
   */
  Object.defineProperty(this, "makeRandomHours", {
    get: function () {
      return function (partition) {

        var hour;
        if( partition && partition.lenth === 2 ) {

          refHours = random(partition[0], partition[1]);

        } else {

          refHours = random(0, 23);

        }

      };
    },
    configurable: false,
    enumerable: false
  });

  Object.defineProperty(this, "setMinutesSeconds", {
    get: function () {
      return function (min, sec) {

        refMinutes = min;
        refSeconds = sec;

        console.log("[setMinutesSeconds] to:", refMinutes, refSeconds);

      };
    },
    configurable: false,
    enumerable: false
  });


  Object.defineProperty(this, "sameDate", {
    get: function () {
      return function () {
        // let lim = dateLimitForMonth(refMonth);
        // let date = random(1, lim);
        return new Date(refYear, refMonth, refDate, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });

  Object.defineProperty(this, "sameMonth", {
    get: function () {
      return function () {
        let lim = dateLimitForMonth(refMonth);
        let date = random(1, lim);
        return new Date(refYear, refMonth, date, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });

  Object.defineProperty(this, "lastMonth", {
    get: function () {
      return function () {
        let pm = subMonths(new Date(refYear, refMonth), 1);

        let m = pm.getMonth();
        let lim = dateLimitForMonth(m);
        let date = random(1, lim);
        return new Date(pm.getFullYear(), m, date, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });

  Object.defineProperty(this, "nextMonth", {
    get: function () {
      return function () {

        let pm = addMonths(new Date(refYear, refMonth), 1);

        let m = pm.getMonth();
        let lim = dateLimitForMonth(m);
        let date = random(1, lim);

        return new Date(pm.getFullYear(), m, date, refHours, refMinutes, refSeconds);
      };
    },
    configurable: false,
    enumerable: false
  });


};

export default MockUpDate;
export { dateLimitForMonth, randomDateForMonth, defaultTimePartition as timePartition };