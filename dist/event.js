"use strict";

var _mockupattr = _interopRequireDefault(require("../mockupattr.js"));

var _mockupgen = _interopRequireDefault(require("../mockupgen.js"));

var _mockup = _interopRequireDefault(require("../mockup.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/// ==========================================
/// Sources
var pool = require('./pool.json');

var address = require('./taiwan.address.json');

var imageSources = ['/r/images/mockups/testimg.jpg', '/r/images/mockups/imagetest.jpg']; /// ==========================================
/// ==========================================
/// Preparing generaters

var MockUpDateRaw = require('../mockupdate.js');

console.log("[MockUpDateRaw]", MockUpDateRaw);
var MockUpDate = MockUpDateRaw["default"];
var timePartition = MockUpDateRaw.timePartition;
/// Make only three cities for simplicity.
var Cities = address.map(function (o) {
  return o.CityName;
}).splice(0, 3);
var city = new _mockupgen["default"](Cities); /// Title

var preposition = new _mockupgen["default"](pool.prepo);
var noun = new _mockupgen["default"](pool.noun); /// Image

var mockUpImage = new _mockupgen["default"](imageSources); /// Date

var mockUpDate = new MockUpDate(new Date());
mockUpDate.setMinutesSeconds(12, 0); // mockUpDate.makeHoursRandom(timePartition.noon);
/// ==========================================
/// Properties

var title = new _mockupattr["default"]("title", function () {
  return preposition.one() + noun.coin(2);
});
var tag = new _mockupattr["default"]("tag", function () {
  return city.one();
});
var datetime = new _mockupattr["default"]("datetime", function () {
  mockUpDate.makeHoursRandom(timePartition.noon);
  return mockUpDate.nextMonth();
});
var image = new _mockupattr["default"]("image", mockUpImage.one); /// ==========================================
/// Mock Up Event Item

var EventItem = new _mockup["default"]([title, tag, datetime, image]);
var list = EventItem.make(99);
console.log("[MockUp] make list:", list);

var fs = require('fs');

fs.writeFile('./event-item-list.json', JSON.stringify(list), function (err) {
  if (err) throw err;
  console.log('complete');
});