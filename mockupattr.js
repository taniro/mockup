"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function MockUpAttr(name, gen) {
  if (!(this instanceof MockUpAttr)) {
    return new MockUpAttr();
  }

  if (typeof gen !== 'function') {
    throw Error("[MockUpAttr] gen must be a function that returns mock-up attribute data.");
  }

  Object.defineProperty(this, "name", {
    get: function get() {
      return name;
    },
    configurable: false,
    enumerable: true
  });
  Object.defineProperty(this, "gen", {
    get: function get() {
      return gen;
    },
    configurable: false,
    enumerable: true
  });
}

var _default = MockUpAttr;
exports["default"] = _default;