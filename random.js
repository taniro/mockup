"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RandomThis = exports.randomPartition = exports.random = exports["default"] = void 0;
/// Default random number generator. The returned value is expected to be between 0 and 1.
var PRNG = Math.random;
/**
 * Generate random number from 0 to 100: randomScalingFactor(100)
 * @param  {[type]} m [description]
 * @return {[Integer]}   [description]
 */

var randomScalingFactor = function randomScalingFactor(m) {
  return Math.round(PRNG() * m);
};
/**
 * Generate random number from f to t: random(f, t)
 * @param  {[type]} f [description]
 * @param  {[type]} t [description]
 * @return {[Integer]}   [description]
 */


var random = function random(f, t) {
  var len = t - f;
  return f + randomScalingFactor(len);
};
/**
 * Random content generator with given pool of source.
 * @param {[Array]} pool Array
 */


exports.random = random;

var RandomThis = function RandomThis(pool) {
  if (!(this instanceof RandomThis)) {
    return new RandomThis(pool);
  }

  if (!pool || !pool.length || pool.length < 1) {
    throw Error("[randomThis] pool must be an array with contents");
  }

  var lim = pool.length - 1;
  Object.defineProperty(this, "gen", {
    get: function get() {
      return function () {
        return pool[randomScalingFactor(lim)];
      };
    },
    configurable: false,
    enumerable: false
  });
};
/**
 * Partition 1 into n decimal fractions.
 * 
 * @param  {[type]} n [description]
 * @return {[type]}   [description]
 */


exports.RandomThis = RandomThis;

var randomPartition = function randomPartition(n) {
  if (isNaN(n)) {
    throw new Error("n should be a number.");
  }

  if (n < 2 || n % 1 != 0) {
    throw new Error("n must be a whole number greater than 1.");
  } /// Make n-1 points


  n--;
  var points = [];

  for (; n--;) {
    points.push(PRNG());
  }

  points.sort(function (a, b) {
    return a - b;
  });
  points.push(1);
  var cursor = 0;
  return points.reduce(function (res, it) {
    res.push(it - cursor);
    cursor = it;
    return res;
  }, []);
};

exports.randomPartition = randomPartition;
var _default = randomScalingFactor;
exports["default"] = _default;