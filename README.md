# MockUp
Mock-up data generator utilies.

This is a personal project that aims for a simple and fast way of generating mock-up data. Use with your own discretion.

## Installation

NPM:
```bash
> npm i @taniro/mockupdata
```

Yarn:
```bash
> yarn add @taniro/mockupdata
```


## Basic Usage

### MockUpGen
The simplest way of making mock-up data is generate them from a single source.

For a json source like this one:
```json
{
  "title1": ["pre", "re", "pseudo"],
  "title2": ["wicked", "awesome", "great", "lame", "shiny"],
  "title3": ["name", "flower", "lake", "pool", "pond", "rock", "king", "mud", "way", "face", "palm", "tree"]
}
```

You can generate a simple value with MockUpGen

```javascript

import source from 'pool.json'
import MockUpGen from '@taniro/mockupdata/mockupgen'

let mud1 = new MockUpGen(source.title2);

// Generate one random data
console.log("[mud1]", mud1.one());
// [mud1] awesome

// Generate mutiple random data
console.log("[mud1]", mud1.make(5));
// [mud1] ["wicked", "awesome", "great", "lame", "shiny"]

// Make 2 random data and coin them into a space-separated phrase.
console.log("[mud1]", mud1.coin(2, " "));
// [mud1] wicked awesome


// Or you can define your own generator function.
let mudFn = new MockUpGen(()=>(new Date).getTime());
expect(mudFn).toBeDefined();

console.log("[mudFn] one", mudFn.one());
// [mud1Fn] one 1599641035832
console.log("[mudFn] coin 2", mudFn.coin(2, " "));
// [mud1Fn] coin 2 1599641035835 1599641035835


```


### MockUp quick start

MockUp can make complex object according to a config object. You define config with desired properties with values

```javascript

    import source from 'pool.json'
    import MockUp from '@taniro/mockupdata'

    var config = {
        title: ["title1","title2","title3"], // Generate title from the list
        subtitle: ()=>{return "whatever"} // Generate subtitle from whatever generator you give it.
    }

    let mu = new MockUp(config);

    console.log("[MockUp]", mu.make(1));
    console.log("[MockUp] make 3:", mu.make(3));


```


## MockUpDate

MockUpDate is a date generator that creates random date according to a reference date.

```javascript

    const MockUpDateRaw = require('@taniro/mockupdata/mockupdate');

    const MockUpDate = MockUpDateRaw.default;
    const timePartition = MockUpDateRaw.timePartition;

    // Use current time as reference date.
    let mudDate = new MockUpDate(new Date());

    // let's say that if current time is 2020/8/9 17:20:20

    // sameMonth creates datetime of the the same month, randomize only date
    console.log("[mudDate] same month", mudDate.sameMonth());
    // [mudDate] same month 
    // Date Thu Sep 24 2020 17:20:20

    console.log("[mudDate] last month", mudDate.lastMonth());
    // [mudDate] last month 
    // Date Fri Aug 14 2020 17:20:20

    console.log("[mudDate] next month", mudDate.nextMonth());
    // [mudDate] next month 
    // Date Sat Oct 24 2020 17:20:20


    // This changes your reference time to 15:00, from now on the reference time will be 17:15:00
    mudDate.setMinutesSeconds(15, 0);

    // This changes your reference hours to a random number between 1~24.
    mudDate.makeRandomHours();
    console.log("[mudDate] same month, random hours", mudDate.sameMonth());
    // [mudDate] same month, random hours 
    // Date Mon Sep 28 2020 02:15:00

    // You can event limit randomized hours to a given range.
    mudDate.makeRandomHours([11, 13]);

    // MuckUpDate comes with a default set of time partitions. This is identical to last code.
    mudDate.makeRandomHours(timePartition.noon);
    console.log("[mudDate] same month, noon hours", mudDate.sameMonth());
    // [mudDate] same month, noon hours 
    // Date Sat Sep 12 2020 04:15:00

```

Default time partition is defined as the following:
```json
{
  "daytime": [7,18],
  "nighttime": [19,23],
  "midnight": [0,6],
  "morning": [7,10],
  "noon": [11,13],
  "afternoon": [12,18],
}
```


## MockUp and MockUpAttr
If you want to generate a more complex object data, use MockUp and MockUpAttr to do the job.

For an object like this:
```json
{
    "title": "abc",
    "subtitle": "def"
}
```

We need to create a MocuUpAttr instance for each of them, and create a MockUp instance with an array of MocuUpAttr.
```javascript

    var gentitle = new MockUpGen(source.title3);

    var gensubpre = new MockUpGen(source.title1);
    var gensub1 = new MockUpGen(source.title2);
    var gensub2 = new MockUpGen(source.title3);

    // Property for title
    let title = new MockUpAttr("title", function(){ return gentitle.coin(2, " ") });

    // Property for subitle
    let subtitle = new MockUpAttr("subtitle", function(){
        return [gensubpre.one() + "-" + gensub2.one()].concat(gensub1.make(2)).concat(gensub2.make(3)).join(" ");
    });

    let mu = new MockUp([
        title,
        subtitle
    ]);

    console.log("[MockUp]", mu.make(1));
    console.log("[MockUp] make 3:", mu.make(3));


```
