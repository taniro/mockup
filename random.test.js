
const randomPartition = require('./random.js').randomPartition;

test('Test randomPartition', () => {


    expect(randomPartition).toBeDefined();

    let n = 7;

    let partitions = randomPartition(n);

    console.log("[partitions]",partitions);

    expect(partitions.length).toBe(n);
    expect(partitions.reduce((res,it)=>res+it,0)).toBe(1);

});
