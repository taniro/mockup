

const MockUpAttr = require('./mockupattr.js').default;

const MockUpDateRaw = require('./mockupdate.js');

const MockUp = require('./mockup.js').default;



console.log("[MockUpAttr]", MockUpDateRaw);

const MockUpDate = MockUpDateRaw.default;
const timePartition = MockUpDateRaw.timePartition;


var mudDate;

test('mockup date creation', () => {

    mudDate = new MockUpDate(new Date());

    expect(mudDate).toBeDefined();

    console.log("[mudDate] same month", mudDate.sameMonth());

    console.log("[mudDate] last month", mudDate.lastMonth());
    console.log("[mudDate] next month", mudDate.nextMonth());

    mudDate.setMinutesSeconds(12, 0);

    mudDate.makeRandomHours();

    console.log("[mudDate] same month, random hours", mudDate.sameMonth());

    mudDate.makeRandomHours(timePartition.noon);
    console.log("[mudDate] same month, noon hours", mudDate.sameMonth());

});


// test('mockupattr creation', () => {

//     var gentitle = new MockUpGen(source.title3);

//     let title = new MockUpAttr("title", function(){ return gentitle.coin(2, " ") });

//     console.log("[title]", title.gen());

// });



// test('mockup creation', () => {

//     var gentitle = new MockUpGen(source.title3);

//     var gensubpre = new MockUpGen(source.title1);
//     var gensub1 = new MockUpGen(source.title2);
//     var gensub2 = new MockUpGen(source.title3);

//     // Property for title
//     let title = new MockUpAttr("title", function(){ return gentitle.coin(2, " ") });


//     // Property for subitle
//     let subtitle = new MockUpAttr("subtitle", function(){
//         return [gensubpre.one() + "-" + gensub2.one()].concat(gensub1.make(2)).concat(gensub2.make(3)).join(" ");
//     });


//     let mu = new MockUp([
//         title,
//         subtitle
//     ]);


//     expect(mu).toBeDefined();


//     console.log("[MockUp]", mu.make(1));
//     console.log("[MockUp] make ten:", mu.make(99));


// });



// test('roster prevent roster with the same name', () => {

//     expect(() => {
//         new roster("list");
//     }).toThrow();

// });


// test('roster creation 2', () => {

//     r2 = new roster("list2");
//     expect(r2).toBeDefined();
//     expect(r2).not.toBe(r1);

// });


// let A = { 'a': '"a" object'};
// let B = { 'b': '"b" object'};

// test('roster API test', () => {

//     expect(r1.register('a', A)).toBe(true);
//     expect(r1.register('b', B)).toBe(true);

//     expect(r1.register('a', ["a"])).toBe(false);

//     expect(r1.valueForKey('a')).toBe(A);
//     expect(r1.valueForKey('b')).toBe(B);

// });


// test('roster instance interference check', () => {

//     let r2B = ["b"];

//     expect(r2.valueForKey('a')).toBeUndefined();

//     expect(r2.register('b', r2B)).toBe(true);
//     expect(r2.valueForKey('b')).toBe(r2B);

// });

// test('roster list test', () => {

//     expect(r1.list).toEqual([A,B]);

// });