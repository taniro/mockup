"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MockUpCTOR = MockUpCTOR;
Object.defineProperty(exports, "MockUpAttr", {
  enumerable: true,
  get: function get() {
    return _mockupattr["default"];
  }
});
exports["default"] = void 0;

var _random = require("./random.js");

var _mockupattr = _interopRequireDefault(require("./mockupattr.js"));

var _mockupgen = _interopRequireDefault(require("./mockupgen.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * [MockUpCTOR description]
 * @param {[type]} properties [description]
 */
function MockUpCTOR(properties) {
  if (!(this instanceof MockUpCTOR)) {
    return new MockUpCTOR(properties);
  } /// ============================================================
  /// Initialization
  /// ============================================================


  if (!properties || !properties.length || properties.length < 1) {
    throw Error("[MockUpCTOR] properties must be an array with contents");
  }

  function gen() {
    console.log("[gen] properties ", properties);
    return properties.reduce(function (result, it) {
      console.log("[gen][reduce] ", it);
      result[it.name] = it.gen();
      return result;
    }, {});
  } /// ============================================================
  /// API
  /// ============================================================

  /**
   * Generate n random data.
   * @return {[type]}               List of mock-up data
   */


  Object.defineProperty(this, "make", {
    get: function get() {
      return function (n) {
        n = n || 1;
        var i, it;
        var list = [];
        i = n;

        for (; i--;) {
          list.push(gen());
        }

        return list;
      };
    },
    configurable: false,
    enumerable: false
  });
}

function MockUp(config) {
  if (config && config.length) {
    return new MockUpCTOR(config);
  }

  if (_typeof(config) == "object") {
    var properties = [];
    console.log("[config]", config);

    for (var prop in config) {
      var pool = config[prop];
      console.log("[config][" + prop + "] pool", pool);

      if (pool) {
        if (typeof pool == "function") {
          properties.push(new _mockupattr["default"](prop, pool));
          continue;
        } else if (pool.length) {
          var mug = new _mockupgen["default"](pool);
          properties.push(new _mockupattr["default"](prop, mug.one));
          continue;
        } else {
          console.warn("[config][" + prop + "] unknown pool type", pool);
        }
      }
    }

    return new MockUpCTOR(properties);
  }

  throw Error("[MockUp] config data must be an object or an array.");
}

var _default = MockUp;
exports["default"] = _default;