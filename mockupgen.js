"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _random = require("./random.js");

function MockUpGen(pool) {
  if (!(this instanceof MockUpGen)) {
    return new MockUpGen(pool);
  } /// ============================================================
  /// Initialization
  /// ============================================================


  var gen;

  if (typeof pool === 'function') {
    gen = pool;
  } else {
    var rt = new _random.RandomThis(pool);
    gen = rt.gen;
  } /// ============================================================
  /// API
  /// ============================================================


  Object.defineProperty(this, "one", {
    get: function get() {
      return function () {
        return gen();
      };
    },
    configurable: false,
    enumerable: false
  });
  /**
   * Generate n random data.
   * @return {[type]}               List of mock-up data
   */

  Object.defineProperty(this, "make", {
    get: function get() {
      return function (n) {
        var i, it;
        var list = [];
        i = n;

        for (; i--;) {
          list.push(gen());
        }

        return list;
      };
    },
    configurable: false,
    enumerable: false
  });
  /**
   * Generate a number of values and combine them as one single value.
   * @return {[String]}               [description]
   */

  Object.defineProperty(this, "coin", {
    get: function get() {
      return function (n, sep) {
        sep = sep || "";
        return this.make(n).join(sep);
      };
    },
    configurable: false,
    enumerable: true
  });
}

var _default = MockUpGen;
exports["default"] = _default;