
const source = require('./pool.json');

const MockUpAttr = require('./mockupattr.js').default;
const MockUpGen = require('./mockupgen.js').default;


var mud1, mud2;

test('mockupdata creation', () => {

    mud1 = new MockUpGen(source.title2);
    expect(mud1).toBeDefined();

    console.log("[mud1]", mud1.coin(2, " "));

});

test('mockupdata creation with function', () => {

    let mud1Fn = new MockUpGen(()=>(new Date).getTime());
    expect(mud1Fn).toBeDefined();

    console.log("[mud1Fn] one", mud1Fn.one());
    console.log("[mud1Fn] coin 2", mud1Fn.coin(2, " "));

});


test('mockupattr creation', () => {

    var gentitle = new MockUpGen(source.title3);

    let title = new MockUpAttr("title", function(){ return gentitle.coin(2, " ") });

    console.log("[title]", title.gen());

});
